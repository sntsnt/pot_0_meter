// pot-0-meter, base_station
// 210824, raphael keusch
// 
// non-standard libs used: 
// RF24 by TMRh20, v 1.4.0
// Adafruit SHT31 Library, v 2.0.0

// supply battery voltage on pin A0, divided by 2

#include <SPI.h>
#include <Wire.h>
#include "printf.h"
#include "RF24.h"
#include "SHTSensor.h"

// Sensor with normal i2c address
// Sensor 1 with address pin pulled to GND
SHTSensor sht1(SHTSensor::SHT3X);

// Sensor with alternative i2c address
// Sensor 2 with address pin pulled to Vdd
SHTSensor sht2(SHTSensor::SHT3X_ALT);

// instantiate an object for the nRF24L01 transceiver
RF24 radio(7, 8); // using pin 7 for the CE pin, and pin 8 for the CSN pin

// Let these addresses be used for the pair
uint8_t address[][6] = {"BASE", "SENS"};
bool radioNumber = 0; // 0 uses address[0] to transmit, 1 uses address[1] to transmit

float payload[] = {0, 0, 0};

void setup() {
  Wire.begin();
  Serial.begin(115200);
  while (!Serial) {
    // some boards need to wait to ensure access to serial over USB
  }

  // initialize the transceiver on the SPI bus
  if (!radio.begin()) {
    Serial.println(F("radio hardware is not responding!!"));
    while (1) {} // hold in infinite loop
  }
  radio.setDataRate(RF24_250KBPS);
  // Set the PA Level low to try preventing power supply related problems
  // because these examples are likely run with nodes in close proximity to
  // each other.
  // radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default.
  radio.setPALevel(RF24_PA_MAX);  // RF24_PA_MAX is default.

  // save on transmission time by setting the radio to only transmit the
  // number of bytes we need to transmit a float
  radio.setPayloadSize(sizeof(payload)); // float datatype occupies 4 bytes

  // set the TX address of the RX node into the TX pipe
  radio.openWritingPipe(address[radioNumber]);     // always uses pipe 0

  // set the RX address of the TX node into a RX pipe
  radio.openReadingPipe(1, address[!radioNumber]); // using pipe 1

  radio.stopListening();  // put radio in TX mode

  // initialize sensor with normal i2c-address
  sht1.init();
  sht1.setAccuracy(SHTSensor::SHT_ACCURACY_HIGH);

  // initialize sensor with alternative i2c-address
  sht2.init();
  sht2.setAccuracy(SHTSensor::SHT_ACCURACY_HIGH);

} // setup


void loop() {
 
  Serial.println("loop");
  // get sensor data
  if (sht1.readSample()) {
    payload[0] = sht1.getTemperature();
  }
  if (sht2.readSample()) {
    payload[1] = sht2.getTemperature();
  }

  // battery voltage  
  payload[2] = 5.0 / 1023 * analogRead(A0)*2; 


  unsigned long start_timer = micros();                    // start the timer
  bool report = radio.write(&payload, sizeof(payload));      // transmit & save the report
  unsigned long end_timer = micros();                      // end the timer

  if (report) {
    Serial.print(F("Transmission successful! "));          // payload was delivered
    Serial.print(F("Sent: "));
    Serial.print(payload[0], 3);                               
    Serial.print(", ");
    Serial.print(payload[1], 3);   
    Serial.print(", ");
    Serial.println(payload[2], 3);   
  } else {
    Serial.println(F("Transmission failed or timed out")); // payload was not delivered
  }

  delay(500);

} // loop

