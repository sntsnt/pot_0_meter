// pot-0-meter, base_station
// 210824, raphael keusch
// 
// non-standard libs used: 
// RF24 by TMRh20, v 1.4.0
// Adafruit GFX Library, v 1.10.10
// Adafruit ILI9341, v 1.5.9
// CircularBuffer from https://github.com/rlogiacco/CircularBuffer, put into /Arduino/libraries folder


#include "SPI.h"
#include "RF24.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
#include <CircularBuffer.h> 

// // soft SPI: working config for ESP8266
// #define TFT_CS D2 
// #define TFT_DC D1 
// #define TFT_MOSI D7 
// #define TFT_CLK D6
// #define TFT_RST D0 // not used, connect to RST
// #define TFT_MISO D5 // not necessairy if you do not want to read data
// Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);


// // hardware SPI: working config for ESP8266
// #define TFT_DC D2
// #define TFT_CS D8
// Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

// // hardware SPI: working config for arduino nano
// #define TFT_DC 9
// #define TFT_CS 10
// Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

// hardware SPI: working config for arduino rf nano
#define TFT_DC 8
#define TFT_CS 10

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);
// comment: other pins are connected to the standard SPI interface, i.e.,
// #define TFT_MOSI 11 
// #define TFT_CLK 13
// #define TFT_RST 8 // not used, connected to 3.3V
// #define TFT_MISO 7 // leave open


// // software SPI: working config for arduino nano
// // where 10 is D10
// #define TFT_CS 10 
// #define TFT_DC 9 
// #define TFT_MOSI 11 
// #define TFT_CLK 13

// #define TFT_RST 8 // not used, connect to RST
// #define TFT_MISO 7 // not necessairy if you do not want to read data
// Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);




// instantiate an object for the nRF24L01 transceiver
RF24 radio(7, 8); // using pin 7 for the CE pin, and pin 8 for the CSN pin

// Let these addresses be used for the pair
uint8_t address[][6] = {"BASE", "SENS"};
bool radioNumber = 1; // 0 uses address[0] to transmit, 1 uses address[1] to transmit

float payload[] = {0, 0, 0};
// colors
#define ILI9341_GRAY 0x4208

// plot
#define PLOT_W 256  // keep this unchanged, native resolution for zoom = 0
#define PLOT_H 170
#define PLOT_OS_X 35
#define PLOT_OS_Y 5 + PLOT_H
#define N_TICKS_Y 6

// heat indicator
#define T_COLD 20
#define T_HOT 30

#define ILI9341_LIGHTBLUE   0x061F      /*   0, 192, 255 */
#define COLOR_1 ILI9341_GREEN
#define COLOR_2 ILI9341_LIGHTBLUE

// buffers
#define LEN_FAST_BUF 64
#define LEN_SLOW_BUF 64
#define DECIMATION 8   // decimation factor from fast to slow buffer

// #define LEN_FAST_BUF 16
// #define LEN_SLOW_BUF 4
// #define DECIMATION 8  // decimation factor from fast to slow buffer


static uint16_t cnt_decimation = 0; // counter for decimation from fast_buf to slow_buf

int8_t zoom = 1; // zoom level,  -2, -1, 0, 1, 2, 3,   0: native


typedef struct{
     int16_t a;
     int16_t b;
} sample;

CircularBuffer<sample,LEN_FAST_BUF> fast_buf;  // fast buffer for most recent data
CircularBuffer<sample,LEN_SLOW_BUF> slow_buf;  // slow buffer for past data

unsigned long time_last_update = 0;
float sensor_bat_voltage = 0.0; // battery voltage of sensor node

void setup() {
  Serial.begin(9600);

  tft.begin();
  tft.fillScreen(ILI9341_BLACK);
  tft.setRotation(1);
  
  tft.setTextColor(ILI9341_RED);  
  tft.setTextSize(3); 
  tft.setCursor(70, 70); 
  tft.println("pot-0-meter");
  tft.setTextColor(ILI9341_WHITE);  
  tft.setTextSize(2); 
  tft.setCursor(70, 100); 
  tft.println("no data :(");
  

  // initialize the transceiver on the SPI bus
  if (!radio.begin()) {
    Serial.println(F("radio hardware is not responding!!"));
    while (1) {} // hold in infinite loop
  }

  radio.setDataRate(RF24_250KBPS);
  // Set the PA Level low to try preventing power supply related problems
  // because these examples are likely run with nodes in close proximity to
  // each other.
  radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default.

  // save on transmission time by setting the radio to only transmit the
  // number of bytes we need to transmit a float
  radio.setPayloadSize(sizeof(payload)); // float datatype occupies 4 bytes

  // set the TX address of the RX node into the TX pipe
  radio.openWritingPipe(address[radioNumber]);     // always uses pipe 0

  // set the RX address of the TX node into a RX pipe
  radio.openReadingPipe(1, address[!radioNumber]); // using pipe 1

  radio.startListening(); // put radio in RX mode

}
 
 
void loop() {   
  uint8_t pipe;              
  // fillBuffer();
  // draw();
  uint8_t update_cnt = 0;
  
  while( true ) {
    if (radio.available(&pipe)) {             // is there a payload? get the pipe number that recieved it
      receivePayload();
      update_cnt++;
      time_last_update = millis();
    }
    draw_aux_info();
    if (update_cnt == 5) {
      draw();
      update_cnt = 0;
      // zoom++;
      // if (zoom == 4)
      //   zoom = -2;
    }    
    delay(100); // resets wdt on esp8266
  }
}

// draws time since last package received
void draw_aux_info(){
  float time_delta;

  tft.setTextColor(ILI9341_GRAY, ILI9341_BLACK);
  tft.setTextSize(1); 
  
  time_delta = (millis() - time_last_update)/1000.0;
  tft.setCursor(280, 230); 
  tft.print(time_delta, 1);
  tft.print("s  ");

}


// draws graph and data
void draw() {
  sample bounds, bounds_round;

  tft.fillScreen(ILI9341_BLACK);

  // auxiliary quanities  
  uint8_t every_nth_s_fast = 1 << max(0, zoom); // take every nth sample of fast buffer
  uint8_t every_nth_s_slow = 1; // take every nth sample of fast buffer 
  uint8_t delta_x_fast = 1 << max(0, -zoom);  // x-distance between to "to-drawn" samples
  uint8_t delta_x_slow = delta_x_fast * DECIMATION / every_nth_s_fast;  

  // Serial.print("every_nth_s_fast: ");  
  // Serial.println(every_nth_s_fast);
  // Serial.print("delta_x_fast: ");  
  // Serial.println(delta_x_fast);
  // Serial.print("delta_x_slow: ");  
  // Serial.println(delta_x_slow);

  uint16_t active_s_fast = fast_buf.size() / every_nth_s_fast; // how many samples of fast_buf will be displayed?
  Serial.print("active_s_fast: ");  
  Serial.println(active_s_fast);
  uint16_t width_of_slow_buf_data = PLOT_W - active_s_fast * delta_x_fast; // what is the remainig plot width for the slow buffer?
  
  uint16_t active_s_slow = min(width_of_slow_buf_data / delta_x_slow, slow_buf.size());  // how many samples of slow_buf will be displayed?
  Serial.print("active_s_slow: ");  
  Serial.println(active_s_slow); 

  // get min and max of samples which are to be plotted
  if (active_s_fast > 0) { // if at least one sample available
    bounds = getMinMax(active_s_fast, every_nth_s_fast, active_s_slow, every_nth_s_slow);
    // Serial.println(bounds.a);
    // Serial.println(bounds.b);


    bounds_round.a = floor(bounds.a / 100.0); // lower bound, rounded to integer
    bounds_round.b = ceil(bounds.b / 100.0); // lower bound, to integer
    drawAxis(bounds_round);
    drawData(active_s_fast, every_nth_s_fast, delta_x_fast, active_s_slow, every_nth_s_slow, delta_x_slow, bounds_round);
  }  
}



// returns min and max of buffers as sample {min, max}
sample getMinMax(uint16_t active_s_fast, uint8_t every_nth_s_fast, uint16_t active_s_slow, uint8_t every_nth_s_slow) {
  sample bounds;
  bounds.a = fast_buf[0].a; // initial min value
  bounds.b = fast_buf[0].a; // initial max value

  for (int ii = 0; ii < active_s_fast*every_nth_s_fast; ii += every_nth_s_fast){
    if (min(fast_buf[ii].a, fast_buf[ii].b) < bounds.a)
      bounds.a = min(fast_buf[ii].a, fast_buf[ii].b);
    if (max(fast_buf[ii].a, fast_buf[ii].b) > bounds.b)
      bounds.b = max(fast_buf[ii].a, fast_buf[ii].b);
  }

  for (int ii = 0; ii < active_s_slow*every_nth_s_slow; ii += every_nth_s_slow){
    if (min(slow_buf[ii].a, slow_buf[ii].b) < bounds.a)
      bounds.a = min(slow_buf[ii].a, slow_buf[ii].b);
    if (max(slow_buf[ii].a, slow_buf[ii].b) > bounds.b)
      bounds.b = max(slow_buf[ii].a, slow_buf[ii].b);
  }
  return bounds;
}


void drawAxis(sample bounds_round) {
  uint16_t y_px;
  uint16_t delta_y_deg = bounds_round.b - bounds_round.a;
  uint8_t how_many_ticks;
  int ii; 
  int16_t  x1, y1;
  uint16_t w, h;
  int16_t timespan;

  tft.setTextColor(ILI9341_WHITE);  tft.setTextSize(1); 

  // calculate ticks spacing
  ii = 1;  // ticks distance in deg
  for (; 1.0 * delta_y_deg / ii > N_TICKS_Y; ii++);
  how_many_ticks = (delta_y_deg-0.0001) / ii;  

  if (how_many_ticks > 0) {  
    for (int mm = 0; mm <= how_many_ticks; mm++){
      y_px = PLOT_OS_Y - int(1.0 * (mm) * ii / delta_y_deg * PLOT_H);
      tft.drawLine(PLOT_OS_X, y_px, PLOT_OS_X + PLOT_W, y_px, ILI9341_GRAY);  // grid line   
      // tft.drawLine(PLOT_OS_X, y_px, PLOT_OS_X+5, y_px, ILI9341_WHITE);  // ticks 

      if (y_px > PLOT_OS_Y - PLOT_H + 8) {      
        tft.setCursor(PLOT_OS_X + PLOT_W + 4, y_px - 3); 
        tft.print(bounds_round.a + mm*ii);
      }
      // print upper bound
      y_px = PLOT_OS_Y - PLOT_H;
      tft.setCursor(PLOT_OS_X + PLOT_W + 4, y_px - 3); 
      tft.print(bounds_round.b);

    }
  } else { // if delta_y_deg == 1 deg, add additional 0.5 marker
    y_px = PLOT_OS_Y - int(0.5 * PLOT_H);
    tft.drawLine(PLOT_OS_X, y_px, PLOT_OS_X + PLOT_W, y_px, ILI9341_GRAY);  // grid line   
    // tft.drawLine(PLOT_OS_X, y_px, PLOT_OS_X+5, y_px, ILI9341_WHITE);  // ticks
    
    y_px = PLOT_OS_Y;
    tft.setCursor(PLOT_OS_X + PLOT_W + 4, y_px - 3); 
    tft.println(bounds_round.a, 1);
    y_px = PLOT_OS_Y - int(0.5 * PLOT_H);
    tft.setCursor(PLOT_OS_X + PLOT_W + 4, y_px - 3); 
    tft.println(bounds_round.a + 0.5, 1);
    y_px = PLOT_OS_Y - PLOT_H;
    tft.setCursor(PLOT_OS_X + PLOT_W + 4, y_px - 3); 
    tft.println(bounds_round.b, 1);
  }
  
  // vertical grid
  tft.drawLine(PLOT_OS_X + PLOT_W/2, PLOT_OS_Y, PLOT_OS_X + PLOT_W/2, PLOT_OS_Y- PLOT_H, ILI9341_GRAY);
  tft.drawLine(PLOT_OS_X + PLOT_W/2, PLOT_OS_Y, PLOT_OS_X + PLOT_W/2, PLOT_OS_Y- 5, ILI9341_WHITE);

  // bounding box
  tft.drawRect(PLOT_OS_X, PLOT_OS_Y, PLOT_W, -PLOT_H-1, ILI9341_WHITE);

  // draw x axis
  tft.setTextColor(ILI9341_WHITE, ILI9341_BLACK);  tft.setTextSize(1); 
  tft.setCursor(PLOT_OS_X - 8, PLOT_OS_Y + 7);

  timespan = 1 << (zoom + 2); 
  tft.print(-timespan);
  tft.print("h ");

  tft.setCursor(PLOT_OS_X + PLOT_W/2 - 8, PLOT_OS_Y + 7); 
  if (timespan > 1) 
    tft.print(-(timespan / 2));
  else
    tft.print(-0.5, 1);
  tft.println("h ");
}


// draws text where the coordinates specify the east / middle point of the bounding box
void drawTextCenteredEastMid(uint16_t x, uint16_t y, char str[]) {
  int16_t  x1, y1;
  uint16_t w, h;

  tft.getTextBounds(str, 0, 0, &x1, &y1, &w, &h);

  tft.setCursor(x - w, y - h/2.0); 
  tft.println(str);

}

void drawData(uint16_t active_s_fast, uint8_t every_nth_s_fast, uint8_t delta_x_fast, uint16_t active_s_slow, uint8_t every_nth_s_slow, uint8_t delta_x_slow, sample bounds_round) {
  uint16_t x_px = PLOT_OS_X + PLOT_W - 2; // holds current x position is pixels
  uint16_t y_px;
  uint16_t delta_y_deg = bounds_round.b - bounds_round.a; // plot height in deg
  sample pt_pr_a, pt_pr_b; // previous points

  // plot fast buffer from right to left
  for (int ii = 0; ii < active_s_fast*every_nth_s_fast; ii += every_nth_s_fast){
    // draw sensor a
    y_px = -1 + PLOT_OS_Y - int((fast_buf[ii].a/100.0 - bounds_round.a) / delta_y_deg * PLOT_H);   
    if (ii > 0) {
      tft.drawLine(x_px, y_px, pt_pr_a.a, pt_pr_a.b, COLOR_1);
    }
    pt_pr_a.a = x_px; pt_pr_a.b = y_px; // save current datapoint for next iteration

    // draw sensor b
    y_px = PLOT_OS_Y - int((fast_buf[ii].b/100.0 - bounds_round.a) / delta_y_deg * PLOT_H);
    if (ii > 0) {
      tft.drawLine(x_px, y_px, pt_pr_b.a, pt_pr_b.b, COLOR_2);
    }
    pt_pr_b.a = x_px; pt_pr_b.b = y_px; // save current datapoint for next iteration

    x_px -= delta_x_fast; // increment x position  
  }

  x_px -= cnt_decimation / every_nth_s_fast * delta_x_fast; // compensates distance fast_buf to slow_buf
  
  // plot slow buffer consecutively
  for (int ii = 0; ii < active_s_slow*every_nth_s_slow; ii += every_nth_s_slow){
    // draw sensor b
    y_px = -1 + PLOT_OS_Y - int((slow_buf[ii].a/100.0 - bounds_round.a) / delta_y_deg * PLOT_H);
    // tft.drawPixel(x_px, y_px, COLOR_1);
    tft.drawLine(x_px, y_px, pt_pr_a.a, pt_pr_a.b, COLOR_1);
    pt_pr_a.a = x_px; pt_pr_a.b = y_px; // save current datapoint for next iteration

    y_px = PLOT_OS_Y - int((slow_buf[ii].b/100.0 - bounds_round.a) / delta_y_deg * PLOT_H);
    // tft.drawPixel(x_px, y_px, COLOR_2);
    tft.drawLine(x_px, y_px, pt_pr_b.a, pt_pr_b.b, COLOR_2);
    pt_pr_b.a = x_px; pt_pr_b.b = y_px; // save current datapoint for next iteration
    x_px -= delta_x_slow;
  }

  tft.setTextSize(3);
  tft.setTextColor(COLOR_1);  
  tft.setCursor(60, 200); 
  tft.print(fast_buf.first().a/100.0, 1);
  // tft.print("C");

  tft.setTextColor(COLOR_2);  
  tft.setCursor(200, 200); 
  tft.print(fast_buf.first().b/100.0, 1);
  // tft.print("C");


  // tft.fillRect(150, 200, 20, 20, COLOR_1);  
  float alpha = min(1, max(0, ((fast_buf.first().a+fast_buf.first().b)/200.0 - T_COLD) / (T_HOT - T_COLD)));
  tft.fillRect(155, 200, 20, 20, getColor(alpha)); 

  // draw battery voltage of sensor node
  tft.setTextColor(ILI9341_GRAY, ILI9341_BLACK);
  tft.setTextSize(1); 

  tft.setCursor(5, 230); 
  tft.print("V_bat: ");
  tft.print(sensor_bat_voltage, 3);
  tft.print("v  ");
 
}
// alpha \in [0, 1]
// alpha = 1 -> red, alpha = 0 -> blue
uint16_t getColor(float alpha) {
  uint16_t col;
  // Serial.println(int((1.0 - alpha) * 128));
  col = (int((alpha) * 31.0) << 11) + int((1.0 -alpha) * 31.0);
  // Serial.println(col);
  return col;
}


void receivePayload(){
  sample s;  
  uint8_t bytes = radio.getPayloadSize(); // get the size of the payload
  radio.read(&payload, bytes);            // fetch payload from FIFO
  Serial.print("Sensor data received: ");
  Serial.print(payload[0]);
  Serial.print(", ");
  Serial.println(payload[1]);
  s.a = floatDegToIntCentiDeg(payload[0]);
  s.b = floatDegToIntCentiDeg(payload[1]);
  sensor_bat_voltage = payload[2];
  addToBuffer(s);
}

void fillBuffer(){   
  sample s; 
  for (int i = 0; i < 256; i++) {
    s.a = floatDegToIntCentiDeg(35 + i/100.0 + 0.25*sin(6.28 * i / 60) + 0.1 * random(0, 100) / 100.0);
    s.b = floatDegToIntCentiDeg(33 + cos(6.28 * i / 60) + 0.1 * random(0, 100) / 100.0);
    addToBuffer(s);
  }
  // printBuffer();
}

void addToBuffer(sample s) {
  static sample last;  // storage for last sample of queue
  bool nof; // flag which is true if no overflow occured 

  last = fast_buf.last(); // save last element in case of an overflow
  nof = fast_buf.unshift(s);
  if (!nof) { // if fast buffer overflows
    if (++cnt_decimation >= DECIMATION) { // if DECIMATION times an overflow occured, ..
      slow_buf.unshift(last); // move last sample of fast buf to slow buf
      cnt_decimation = 0; // reset counter
    }
  }
}


// prints both buffers to serial
void printBuffer() {
  Serial.print("[");
  for (int i = 0; i < LEN_FAST_BUF; i++){
    Serial.print("{");
    Serial.print(fast_buf[i].a);
    Serial.print(",");
    Serial.print(fast_buf[i].b);
    Serial.print("}");
      if (i < LEN_FAST_BUF - 1)
    Serial.print(",");
  }
  Serial.print("], [");
  for (int i = 0; i < LEN_SLOW_BUF; i++){
    Serial.print("{");
    Serial.print(slow_buf[i].a);
    Serial.print(",");
    Serial.print(slow_buf[i].b);
    Serial.print("}");
    if (i < LEN_FAST_BUF - 1)
      Serial.print(",");
  }
  Serial.println("]");
}

// converts e.g. 35.67 to 357, and -10.56 to -106
int16_t floatDegToIntCentiDeg(float temp) {
  float sign = 1;
  if (temp < 0) {sign = -1;}
  return (temp * 100 + sign*0.5);  
}
