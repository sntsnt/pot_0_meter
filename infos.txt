Board:
------
RF nano
infos: https://www.fambach.net/rf-nano/

https://github.com/emakefun/emakefun-nano-plus/tree/master/RF-Nano



To buy:

- RF nano with NRF24 with proper antenna

- abstandsbolzen              https://www.bastelgarage.ch/mechanische-bauteile/distanzbolzen/sechskant-m3-distanzbolzen-abstandhalter-messing-120-stuck

- schrauben

- ladegeraet                  https://www.bastelgarage.ch/mp2636-5v-step-up-li-ion-akku-ladegerat

- lipo & batteriehalter       https://www.bastelgarage.ch/li-ion-akku-3000ma-18650-mit-schutzelektronik-und-stecker?search=18650  
  77 x 22 x 21mm

- schraubklemmen              https://www.bastelgarage.ch/bauteile/stecker-klemmen/2p-printklemme-mit-schraubanschluss

- wasserdichtes gehaeuse      https://www.bastelgarage.ch/mechanische-bauteile/gehause/110x80x45mm-ip67-kunststoffgehause-transparent
                              https://www.bastelgarage.ch/mechanische-bauteile/gehause/kunststoffgehause-gehause-ip66-transparent-s